import React from 'react';
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Collapse,
} from 'reactstrap';

const collideasFormCard = (props) => {

  return (
      <Card>
        <CardHeader>
          <i className="fa fa-edit"></i>{props.cardHeader}
          {props.isAction ?
          <div className="card-header-actions">
            <Button color="link" className="card-header-action btn-minimize" data-target="#collapseExample" onClick={props.toggle}><i className={props.isOpen ? 'icon-arrow-down' : 'icon-arrow-down'}></i></Button>
          </div> : null}
        </CardHeader>
        <Collapse isOpen={props.isOpen} id="collapseExample">
          <CardBody>
              {props.children}
          </CardBody>
        </Collapse>
      </Card>
  );

};

export default collideasFormCard;
