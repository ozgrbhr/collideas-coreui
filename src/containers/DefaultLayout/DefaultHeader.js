import React, { Component } from 'react';
import { connect } from 'react-redux';

import Aux from '../../hoc/Auxiliary';

import { Badge, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem, NavLink } from 'reactstrap';
import PropTypes from 'prop-types';

import { AppAsideToggler, AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from '../../assets/img/brand/collideas-logo.png'
import sygnet from '../../assets/img/brand/sygnet.svg'

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    let navLeftItems = null;
    let navRightItems = null;
    if(this.props.isAuthenticated) {
      navLeftItems = (
        <Aux>
          <NavItem className="px-2">
            <NavLink href="#/dashboard">Dashboard</NavLink>
          </NavItem>
          <NavItem className="px-2">
            <NavLink href="#/users">Users</NavLink>
          </NavItem>
          <NavItem className="px-2">
            <NavLink href="#/home">Home</NavLink>
          </NavItem>
          <NavItem className="px-2">
            <NavLink href="#/myaccount">My Account</NavLink>
          </NavItem>
          <NavItem className="px-2">
            <NavLink href="#/myuserinfos">My User Infos</NavLink>
          </NavItem>
        </Aux>
      );
      navRightItems = (
        <NavItem className="d-md-down-none">
          <NavLink href="#/logout"><i className="fa fa-sign-in"></i>&nbsp;Logout&nbsp;&nbsp;</NavLink>
        </NavItem>
      );
    }
    else {
      navLeftItems = (
        <NavItem className="px-2">
          <NavLink href="#/home">Home</NavLink>
        </NavItem>
      );
      navRightItems = (
        <Aux>
          <NavItem className="d-md-down-none">
            <NavLink href="#/mylogin"><i className="fa fa-sign-in"></i>&nbsp;Log In&nbsp;&nbsp;</NavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <NavLink href="#/myregister"><i className="fa fa-user-plus"></i>&nbsp;Sign Up&nbsp;</NavLink>
          </NavItem>
        </Aux>
      );
    }

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 89, height: 25, alt: 'Collideas Logo' }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: 'Collideas Logo' }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="d-md-down-none" navbar>
          {navLeftItems}
        </Nav>
        <Nav className="ml-auto" navbar>
          {navRightItems}
          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              <img src={'assets/img/avatars/6.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com" />
            </DropdownToggle>
            <DropdownMenu right style={{ right: 'auto' }}>
              <DropdownItem header tag="div" className="text-center"><strong>Account</strong></DropdownItem>
              <DropdownItem><i className="fa fa-bell-o"></i> Updates<Badge color="info">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-envelope-o"></i> Messages<Badge color="success">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-tasks"></i> Tasks<Badge color="danger">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-comments"></i> Comments<Badge color="warning">42</Badge></DropdownItem>
              <DropdownItem header tag="div" className="text-center"><strong>Settings</strong></DropdownItem>
              <DropdownItem><i className="fa fa-user"></i> Profile</DropdownItem>
              <DropdownItem><i className="fa fa-wrench"></i> Settings</DropdownItem>
              <DropdownItem><i className="fa fa-usd"></i> Payments<Badge color="secondary">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-file"></i> Projects<Badge color="primary">42</Badge></DropdownItem>
              <DropdownItem divider />
              <DropdownItem><i className="fa fa-shield"></i> Lock Account</DropdownItem>
              <DropdownItem><i className="fa fa-lock"></i> Logout</DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>
        <AppAsideToggler className="d-md-down-none" />
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null
  };
};

export default connect(mapStateToProps, null) (DefaultHeader);
