import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import * as actions from '../../store/actions/index';
import { updateObject, checkValidity } from '../../shared/utility';

import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import Spinner from '../../components/UI/Spinner';

class Login extends Component {

  state = {
    loginForm: {
      email: {
        elementType: 'input',
        elementConfig: {
          type: 'email',
          placeholder: 'E-Posta adresiniz..'
        },
        value: '',
        validation: {
          required: true,
          isEmail: true
        },
        valid: false,
      },
      password: {
        elementType: 'input',
        elementConfig: {
          type: 'password',
          placeholder: 'Parolanız..'
        },
        value: '',
        validation: {
          required: true,
          minLength: 6
        },
        valid: false,
      }
    },
  };

  componentDidMount() {
    if ( this.props.authRedirectPath !== '/home' ) {
      this.props.onSetAuthRedirectPath();
    }
  }

  registerButtonHandler = () => {
    this.props.history.push('/myregister');
  }

  inputChangedHandler = (event, inputIdentifier) => {
    const updatedLoginForm = updateObject(this.state.loginForm, {
      [inputIdentifier]: updateObject( this.state.loginForm[inputIdentifier], {
        value: event.target.value,
        valid: checkValidity( event.target.value, this.state.loginForm[inputIdentifier].validation)
      })
    });

    this.setState({loginForm: updatedLoginForm});
  }

  submitHandler = (event) => {
    event.preventDefault();

    this.props.onAuth(this.state.loginForm.email.value, this.state.loginForm.password.value, false);
  }

  render() {
    let spinner = null;
    if (this.props.loading) {
      spinner = <Spinner />
    }

    let errorMessage = null;
    if (this.props.error) {
      errorMessage = (
        <p style={{color: 'red'}}>{this.props.error.message}</p>
      );
    }

    let authRedirect = null;
    if(this.props.isAuthenticated) {
      authRedirect = <Redirect to={this.props.authRedirectPath} />
    }

    return (
      <div className="app flex-row align-items-center">
        {authRedirect}
        <Container>
          {spinner}
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form onSubmit={this.submitHandler}>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      {errorMessage}
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>@</InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="email"
                          placeholder="Email"
                          autoComplete="email"
                          onChange={(event) => this.inputChangedHandler(event, 'email')}/>
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="password"
                          placeholder="Password"
                          autoComplete="current-password"
                          onChange={(event) => this.inputChangedHandler(event, 'password')}/>
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4">Login</Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">Forgot password?</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: 44 + '%' }}>
                  <CardBody className="text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p>
                      <Button
                        color="primary"
                        className="mt-3"
                        active
                        onClick={this.registerButtonHandler}>Register Now!</Button>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated: state.auth.token !== null,
    authRedirectPath: state.auth.authRedirectPath
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAuth: (email, password, isSignup) => dispatch(actions.auth(email, password, isSignup)),
    onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath('/home'))
  };
};

export default connect(mapStateToProps, mapDispatchToProps) (Login);
