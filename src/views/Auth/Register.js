import React, { Component } from 'react';
import {connect} from "react-redux";
import { Redirect } from 'react-router-dom';

import { Button, Card, CardBody, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import {checkValidity, updateObject} from "../../shared/utility";
import * as actions from "../../store/actions";
import Spinner from '../../components/UI/Spinner';

class Register extends Component {

  state = {
    registerForm: {
      email: {
        elementType: 'input',
        elementConfig: {
          type: 'email',
          placeholder: 'E-Posta adresiniz..'
        },
        value: '',
        validation: {
          required: true,
          isEmail: true
        },
        valid: false,
      },
      password: {
        elementType: 'input',
        elementConfig: {
          type: 'password',
          placeholder: 'Parolanız..'
        },
        value: '',
        validation: {
          required: true,
          minLength: 6
        },
        valid: false,
      }
    },
  };

  componentDidMount() {
    if ( this.props.authRedirectPath !== '/home' ) {
      this.props.onSetAuthRedirectPath();
    }
  }

  loginButtonHandler = () => {
    this.props.history.push('/mylogin');
  }

  inputChangedHandler = (event, inputIdentifier) => {
    const updatedLoginForm = updateObject(this.state.registerForm, {
      [inputIdentifier]: updateObject( this.state.registerForm[inputIdentifier], {
        value: event.target.value,
        valid: checkValidity( event.target.value, this.state.registerForm[inputIdentifier].validation)
      })
    });

    this.setState({registerForm: updatedLoginForm});
  }

  submitHandler = (event) => {
    event.preventDefault();

    this.props.onAuth(this.state.registerForm.email.value, this.state.registerForm.password.value, true);
  }

  render() {
    let spinner = null;
    if (this.props.loading) {
      spinner = <Spinner />
    }

    let errorMessage = null;
    if (this.props.error) {
      errorMessage = (
        <p style={{color: 'red'}}>{this.props.error.message}</p>
      );
    }

    let authRedirect = null;
    if(this.props.isAuthenticated) {
      authRedirect = <Redirect to={this.props.authRedirectPath} />
    }

    return (
      <div className="app flex-row align-items-center">
        {authRedirect}
        <Container>
          {spinner}
          <Row className="justify-content-center">
            <Col md="6">
              <Card className="mx-4">
                <CardBody className="p-4">
                  <Form onSubmit={this.submitHandler}>
                    <h1>Register</h1>
                    <p className="text-muted">Create your account</p>
                    {errorMessage}
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>@</InputGroupText>
                      </InputGroupAddon>
                      <Input
                        type="text"
                        placeholder="Email"
                        autoComplete="email"
                        onChange={(event) => this.inputChangedHandler(event, 'email')}/>
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        type="password"
                        placeholder="Password"
                        autoComplete="new-password"
                        onChange={(event) => this.inputChangedHandler(event, 'password')}/>
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" placeholder="Repeat password" autoComplete="new-password" />
                    </InputGroup>
                    <Button color="success" block>Create Account</Button>
                  </Form>
                  <div style={{height: '8px'}}></div>
                  <p className="text-center">OR</p>
                  <Button
                    color="primary"
                    block
                    onClick={this.loginButtonHandler}>Login Now!</Button>
                </CardBody>
                {/*<CardFooter className="p-4">*/}
                  {/*<Row>*/}
                    {/*<Col xs="12" sm="6">*/}
                      {/*<Button className="btn-facebook" block><span>facebook</span></Button>*/}
                    {/*</Col>*/}
                    {/*<Col xs="12" sm="6">*/}
                      {/*<Button className="btn-twitter" block><span>twitter</span></Button>*/}
                    {/*</Col>*/}
                  {/*</Row>*/}
                {/*</CardFooter>*/}
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated: state.auth.token !== null,
    authRedirectPath: state.auth.authRedirectPath
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAuth: (email, password, isSignup) => dispatch(actions.auth(email, password, isSignup)),
    onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath('/home'))
  };
};

export default connect(mapStateToProps, mapDispatchToProps) (Register);
