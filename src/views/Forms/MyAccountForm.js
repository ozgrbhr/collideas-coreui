import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import {
  Button,
  Col,
  Fade,
  Form,
  FormGroup,
  FormText,
  Input,
  Label,
  Row,
} from 'reactstrap';

import CollideasFormCard from "../../components/Cards/CollideasFormCard";

import { updateObject, checkValidity } from '../../shared/utility';
import {connect} from "react-redux";

class MyAccountForm extends Component {

  state = {
    accountForm: {
      personalInformationsCard: {
        cardHeader: 'View and Edit Your Informations',
        isOpen: true,
        inputs: {
          email: {
            type: 'email',
            value: '',
            validation: {
              required: false
            },
            label: ''
          },
          firstname: {
            type: 'text',
            placeholder: 'Enter your first name',
            value: '',
            validation: {
              required: true
            },
            valid: false
          },
          lastname: {
            type: 'text',
            placeholder: 'Enter your last name',
            value: '',
            validation: {
              required: true
            },
            valid: false
          },
          contactEmail: {
            type: 'email',
            placeholder: 'Enter your contact email',
            value: '',
            validation: {
              required: true
            },
            valid: false
          },
          phone: {
            type: 'text',
            placeholder: 'Enter your phone number',
            value: '',
            validation: {
              required: true
            },
            valid: false
          },
          country: {
            type: 'select',
            options: [
              {value: 'turkey', displayValue: 'Turkey'},
              {value: 'spain', displayValue: 'Spain'}
            ],
            value: '',
            validation: {},
            valid: false
          },
          city: {
            type: 'select',
            options: [
              {value: 'karabuk', displayValue: 'Karabuk'},
              {value: 'ankara', displayValue: 'Ankara'}
            ],
            value: '',
            validation: {},
            valid: false
          }
        }
      }
    },
    fadeIn: true,
    timeout: 300
  };

  // inputChangedHandler = (event, inputIdentifier) => {
  //   const updatedFormElement = updateObject(this.state.accountForm[inputIdentifier], {
  //     value: event.target.value
  //   });
  //
  //   const updatedAccountForm = updateObject(this.state.accountForm, {
  //     [inputIdentifier]: updatedFormElement
  //   });
  //
  //   this.setState({accountForm: updatedAccountForm});
  // }
  //
  // actionFormHandler = (event) => {
  //   event.preventDefault();
  //
  // }

  inputChangedHandler = (event, cardIdentifier, inputIdentifier) => {
    debugger;
    let value = null ;
    if(this.state.accountForm[cardIdentifier].inputs[inputIdentifier].type === 'appswitch')
      value = !this.state.accountForm[cardIdentifier].inputs[inputIdentifier].value;
    else // text
      value = event.target.value;

    const updatedCardInput = updateObject(this.state.accountForm[cardIdentifier].inputs[inputIdentifier], {
      value: value
    });

    const updatedCardInputs = updateObject(this.state.accountForm[cardIdentifier].inputs, {
      [inputIdentifier]: updatedCardInput
    });

    const updatedCard = updateObject(this.state.accountForm[cardIdentifier], {
      inputs: updatedCardInputs
    });

    const updatedUserInfosForm = updateObject(this.state.accountForm, {
      [cardIdentifier]: updatedCard
    });

    this.setState({accountForm: updatedUserInfosForm});
  }

  actionFormHandler = (event) => {
    event.preventDefault();
    console.log(this.state.accountForm.personalInformationsCard.inputs.firstname.value);
    console.log(this.state.accountForm.personalInformationsCard.inputs.lastname.value);
    console.log(this.state.accountForm.personalInformationsCard.inputs.contactEmail.value);
    console.log(this.state.accountForm.personalInformationsCard.inputs.phone.value);
    console.log(this.state.accountForm.personalInformationsCard.inputs.country.value);
    console.log(this.state.accountForm.personalInformationsCard.inputs.city.value);

  }

  render() {

    let redirectToHome = null;
    if(!this.props.isAuthenticated)
      redirectToHome = <Redirect to="/home"/>

    return (
      <div className="animated fadeIn">
        {redirectToHome}
        <Row>
          <Col xs="12">
            <Fade timeout={this.state.timeout} in={this.state.fadeIn}>
              <Form className="form-horizontal" onSubmit={this.actionFormHandler}>
                <CollideasFormCard
                  cardHeader={this.state.accountForm.personalInformationsCard.cardHeader}
                  isOpen={this.state.accountForm.personalInformationsCard.isOpen}
                  toggle={() => this.toggle('personalInformationsCard')}
                  isAction={false}>
                    <FormGroup row className="my-0">
                      <Col xs="12" sm="6">
                        <FormGroup>
                          <Label htmlFor="email-input">Email</Label>
                          <Input
                            type={this.state.accountForm.personalInformationsCard.inputs.email.type}
                            id={'email'}
                            placeholder={this.state.accountForm.personalInformationsCard.inputs.email.placeholder}
                            value={this.state.accountForm.personalInformationsCard.inputs.email.value}
                            // onChange={(event) => this.inputChangedHandler(event, 'personalInformationsCard','email')}
                            disabled/>
                        </FormGroup>
                      </Col>
                    </FormGroup>
                    <FormGroup row className="my-0">
                      <Col xs="12" sm="6">
                        <FormGroup>
                          <Label htmlFor="firstname">First Name</Label>
                          <Input
                            type={this.state.accountForm.personalInformationsCard.inputs.firstname.type}
                            id={'firstname'}
                            placeholder={this.state.accountForm.personalInformationsCard.inputs.firstname.placeholder}
                            value={this.state.accountForm.personalInformationsCard.inputs.firstname.value}
                            onChange={(event) => this.inputChangedHandler(event, 'personalInformationsCard', 'firstname')}/>
                        </FormGroup>
                      </Col>
                      <Col xs="12" sm="6">
                        <FormGroup>
                          <Label htmlFor="lastname">Last Name</Label>
                          <Input
                            type={this.state.accountForm.personalInformationsCard.inputs.lastname.type}
                            id={'lastname'}
                            placeholder={this.state.accountForm.personalInformationsCard.inputs.lastname.placeholder}
                            value={this.state.accountForm.personalInformationsCard.inputs.lastname.value}
                            onChange={(event) => this.inputChangedHandler(event, 'personalInformationsCard', 'lastname')}/>
                        </FormGroup>
                      </Col>
                    </FormGroup>
                    <FormGroup row className="my-0">
                      <Col xs="12" sm="6">
                        <FormGroup>
                          <Label htmlFor="contactEmail">Contact Email</Label>
                          <Input
                            type={this.state.accountForm.personalInformationsCard.inputs.contactEmail.type}
                            id={'contactEmail'}
                            placeholder={this.state.accountForm.personalInformationsCard.inputs.contactEmail.placeholder}
                            value={this.state.accountForm.personalInformationsCard.inputs.contactEmail.value}
                            onChange={(event) => this.inputChangedHandler(event, 'personalInformationsCard', 'contactEmail')}/>
                          <FormText className="help-block">This is the email we'll use to contact you.</FormText>
                        </FormGroup>
                      </Col>
                      <Col xs="12" sm="6">
                        <FormGroup>
                          <Label htmlFor="phone">Phone</Label>
                          <Input
                            type={this.state.accountForm.personalInformationsCard.inputs.phone.type}
                            id={'phone'}
                            placeholder={this.state.accountForm.personalInformationsCard.inputs.phone.placeholder}
                            value={this.state.accountForm.personalInformationsCard.inputs.phone.value}
                            onChange={(event) => this.inputChangedHandler(event, 'personalInformationsCard', 'phone')}/>
                        </FormGroup>
                      </Col>
                    </FormGroup>
                    <FormGroup row className="my-0">
                      <Col xs="12" sm="6">
                        <FormGroup>
                          <Label htmlFor="country">Country</Label>
                          <Input
                            type={this.state.accountForm.personalInformationsCard.inputs.country.type}
                            id="country">
                            value={this.state.accountForm.personalInformationsCard.inputs.country.value}>
                            {this.state.accountForm.personalInformationsCard.inputs.country.options.map(option => (
                              <option key={option.value} value={option.value}>
                                {option.displayValue}
                              </option>
                            ))}
                          </Input>
                        </FormGroup>
                      </Col>
                      <Col xs="12" sm="6">
                        <FormGroup>
                          <Label htmlFor="city">City</Label>
                          <Input
                            type={this.state.accountForm.personalInformationsCard.inputs.city.type}
                            id="city">
                            value={this.state.accountForm.personalInformationsCard.inputs.city.value}>
                            {this.state.accountForm.personalInformationsCard.inputs.city.options.map(option => (
                              <option key={option.value} value={option.value}>
                                {option.displayValue}
                              </option>
                            ))}
                          </Input>
                        </FormGroup>
                      </Col>
                    </FormGroup>
                    <div>
                      <Button type="submit" color="primary">Save changes</Button>
                      {' '}
                      <Button color="secondary">Cancel</Button>
                    </div>
                </CollideasFormCard>
              </Form>
            </Fade>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null
  };
};

export default connect(mapStateToProps, null) (MyAccountForm);
