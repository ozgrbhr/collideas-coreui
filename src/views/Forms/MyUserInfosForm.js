import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import {
  Col,
  Fade,
  Row,
  FormGroup,
  Label,
  Input,
  Form,
  Button,
} from 'reactstrap';
import { AppSwitch } from '@coreui/react';


import { updateObject, checkValidity } from '../../shared/utility';
import CollideasFormCard from "../../components/Cards/CollideasFormCard";
import {connect} from "react-redux";

class MyUserInfosForm extends Component {

  state = {
    userInfosForm: {
      keywordsCard: {
        cardHeader: 'View and Edit Your Keywords',
        isOpen: true,
        inputs: {
          firstKeyword: {
            type: 'text',
            placeholder: 'Enter your first keyword',
            value: '',
            validation: {
              required: true,
              minLength: 3,
              maxLength: 20
            },
            valid: false
          },
          secondKeyword: {
            type: 'text',
            placeholder: 'Enter your first second keyword',
            value: '',
            validation: {
              required: true,
              minLength: 3,
              maxLength: 20
            },
            valid: false
          },
          thirdKeyword: {
            type: 'text',
            placeholder: 'Enter your first third keyword',
            value: '',
            validation: {
              required: true,
              minLength: 3,
              maxLength: 20
            },
            valid: false
          },
          fourthKeyword: {
            type: 'text',
            placeholder: 'Enter your first fourth keyword',
            value: '',
            validation: {
              required: true,
              minLength: 3,
              maxLength: 20
            },
            valid: false
          },
          fifthKeyword: {
            type: 'text',
            placeholder: 'Enter your first fifth keyword',
            value: '',
            validation: {
              required: true,
              minLength: 3,
              maxLength: 20
            },
            valid: false
          }
        },

      },
      collaborationCard: {
        cardHeader: 'View and Edit Your Collaboration',
        isOpen: false,
        inputs: {
          agriculturalScience: {
            type: 'appswitch',
            value: false
          },
          economics: {
            type: 'appswitch',
            value: false
          },
          law: {
            type: 'appswitch',
            value: false
          },
          physics: {
            type: 'appswitch',
            value: false
          },
          anthropology: {
            type: 'appswitch',
            value: false
          },
          education: {
            type: 'appswitch',
            value: false
          },
          linguistics: {
            type: 'appswitch',
            value: false
          },
          politicalScience: {
            type: 'appswitch',
            value: false
          },
          biology: {
            type: 'appswitch',
            value: false
          },
          engineering: {
            type: 'appswitch',
            value: false
          },
          literature: {
            type: 'appswitch',
            value: false
          },
          psychology: {
            type: 'appswitch',
            value: false
          },
          chemistry: {
            type: 'appswitch',
            value: false
          },
          entertainmentAndArts: {
            type: 'appswitch',
            value: false
          },
          mathematics: {
            type: 'appswitch',
            value: false
          },
          socialSciences: {
            type: 'appswitch',
            value: false
          },
          computerSciences: {
            type: 'appswitch',
            value: false
          },
          geoscience: {
            type: 'appswitch',
            value: false
          },
          medicine: {
            type: 'appswitch',
            value: false
          },
          spaceSicence: {
            type: 'appswitch',
            value: false
          },
          design: {
            type: 'appswitch',
            value: false
          },
          history: {
            type: 'appswitch',
            value: false
          },
          philosophy: {
            type: 'appswitch',
            value: false
          },
          other: {
            type: 'appswitch',
            value: false
          }
        }
      },
      availabilitySectorsCard: {
        cardHeader: 'View and Edit Your Availability Sectors',
        isOpen: false,
        inputs: {
          academia: {
            type: 'appswitch',
            value: false
          },
          industry: {
            type: 'appswitch',
            value: false
          },
          government: {
            type: 'appswitch',
            value: false
          },
          civilSociety: {
            type: 'appswitch',
            value: false
          },
          allSectors: {
            type: 'appswitch',
            value: false
          }
        }
      },
      dataCard: {
        cardHeader: 'View and Edit Your Data',
        isOpen: false,
        inputs: {
          available: {
            type: 'appswitch',
            value: false
          },
          toBeCollected: {
            type: 'appswitch',
            value: false
          }
        }
      },
      methodCard: {
        cardHeader: 'View and Edit Your Method',
        isOpen: false,
        inputs: {
          quantitative: {
            type: 'appswitch',
            value: false
          },
          qualitative: {
            type: 'appswitch',
            value: false
          },
          mixed: {
            type: 'appswitch',
            value: false
          },
          allMethods: {
            type: 'appswitch',
            value: false
          }
        }
      },
      scopeOfInvolvementCard: {
        cardHeader: 'View and Edit Your Scope of Involvement',
        isOpen: false,
        inputs: {
          overall: {
            type: 'appswitch',
            value: false
          },
          literature: {
            type: 'appswitch',
            value: false
          },
          analysis: {
            type: 'appswitch',
            value: false
          },
          interpretation: {
            type: 'appswitch',
            value: false
          }
        }
      },
      sequencingCard: {
        cardHeader: 'View and Edit Your Sequencing',
        isOpen: false,
        inputs: {
          firstAuthor: {
            type: 'appswitch',
            value: false
          },
          decideLater: {
            type: 'appswitch',
            value: false
          },
          doesnotmatter: {
            type: 'appswitch',
            value: false
          }
        }
      },
      numberOfCoauthorsCard: {
        cardHeader: 'View and Edit Your Number of Co-authors',
        isOpen: false,
      },
      timeFrameCard: {
        cardHeader: 'View and Edit Your Time Frame',
        isOpen: false,
        inputs: {
          oneToThreeMonths: {
            type: 'appswitch',
            value: false
          },
          sixMonthsToOneYear: {
            type: 'appswitch',
            value: false
          },
          onePlusYear: {
            type: 'appswitch',
            value: false
          }
        }
      }
    },
    fadeIn: true,
    timeout: 300
  };

  toggle(cardName) {
    const updatedFormElement = updateObject(this.state.userInfosForm[cardName], {
      isOpen: !this.state.userInfosForm[cardName].isOpen,
    });

    const updatedUserInfosForm = updateObject(this.state.userInfosForm, {
      [cardName]: updatedFormElement
    });

    this.setState({userInfosForm: updatedUserInfosForm});
  }

  inputChangedHandler = (event, cardIdentifier, inputIdentifier) => {
    let value = null ;
    if(this.state.userInfosForm[cardIdentifier].inputs[inputIdentifier].type === 'appswitch')
      value = !this.state.userInfosForm[cardIdentifier].inputs[inputIdentifier].value;
    else // text
      value = event.target.value;

    const updatedCardInput = updateObject(this.state.userInfosForm[cardIdentifier].inputs[inputIdentifier], {
      value: value
    });

    const updatedCardInputs = updateObject(this.state.userInfosForm[cardIdentifier].inputs, {
      [inputIdentifier]: updatedCardInput
    });

    const updatedCard = updateObject(this.state.userInfosForm[cardIdentifier], {
      inputs: updatedCardInputs
    });

    const updatedUserInfosForm = updateObject(this.state.userInfosForm, {
      [cardIdentifier]: updatedCard
    });

    this.setState({userInfosForm: updatedUserInfosForm});
  }

  userInfosFormHandler = (event) => {
    event.preventDefault();
    console.log(this.state.userInfosForm.keywordsCard.inputs.firstKeyword.value);
    console.log(this.state.userInfosForm.keywordsCard.inputs.secondKeyword.value);
    console.log(this.state.userInfosForm.keywordsCard.inputs.thirdKeyword.value);
    console.log(this.state.userInfosForm.keywordsCard.inputs.fourthKeyword.value);
    console.log(this.state.userInfosForm.keywordsCard.inputs.fifthKeyword.value);
    console.log(this.state.userInfosForm.collaborationCard.inputs.agriculturalScience.value);
    console.log(this.state.userInfosForm.availabilitySectorsCard.inputs.academia.value);
    console.log(this.state.userInfosForm.dataCard.inputs.available.value);
    console.log(this.state.userInfosForm.methodCard.inputs.quantitative.value);
    console.log(this.state.userInfosForm.scopeOfInvolvementCard.inputs.overall.value);
    console.log(this.state.userInfosForm.sequencingCard.inputs.firstAuthor.value);
    console.log(this.state.userInfosForm.timeFrameCard.inputs.oneToThreeMonths.value);
  }

  render() {

    let redirectToHome = null;
    if(!this.props.isAuthenticated)
      redirectToHome = <Redirect to="/home"/>

    return (
      <div className="animated fadeIn">
        {redirectToHome}
        <Fade timeout={this.state.timeout} in={this.state.fadeIn}>
          <Form className="form-horizontal" onSubmit={this.userInfosFormHandler}>
            <Row>
              <Col xs="12">
                <CollideasFormCard
                  cardHeader={this.state.userInfosForm.keywordsCard.cardHeader}
                  isOpen={this.state.userInfosForm.keywordsCard.isOpen}
                  toggle={() => this.toggle('keywordsCard')}
                  isAction>
                  <FormGroup row className="my-0">
                    <Col xs="12" sm="4">
                      <FormGroup>
                        <Label htmlFor="firstKeyword">1. Keyword</Label>
                        <Input
                          type={this.state.userInfosForm.keywordsCard.inputs.firstKeyword.type}
                          id="firstKeyword"
                          placeholder={this.state.userInfosForm.keywordsCard.inputs.firstKeyword.placeholder}
                          value={this.state.userInfosForm.keywordsCard.inputs.firstKeyword.value}
                          onChange={(event) => this.inputChangedHandler(event, 'keywordsCard', 'firstKeyword')}/>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="4">
                      <FormGroup>
                        <Label htmlFor="secondKeyword">2. Keyword</Label>
                        <Input
                          type={this.state.userInfosForm.keywordsCard.inputs.secondKeyword.type}
                          id="secondKeyword"
                          placeholder={this.state.userInfosForm.keywordsCard.inputs.secondKeyword.placeholder}
                          value={this.state.userInfosForm.keywordsCard.inputs.secondKeyword.value}
                          onChange={(event) => this.inputChangedHandler(event, 'keywordsCard', 'secondKeyword')}/>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="4">
                      <FormGroup>
                        <Label htmlFor="thirdKeyword">3. Keyword</Label>
                        <Input
                          type={this.state.userInfosForm.keywordsCard.inputs.thirdKeyword.type}
                          id="thirdKeyword"
                          placeholder={this.state.userInfosForm.keywordsCard.inputs.thirdKeyword.placeholder}
                          value={this.state.userInfosForm.keywordsCard.inputs.thirdKeyword.value}
                          onChange={(event) => this.inputChangedHandler(event, 'keywordsCard', 'thirdKeyword')}/>
                      </FormGroup>
                    </Col>
                  </FormGroup>
                  <FormGroup row className="my-0">
                    <Col xs="12" sm="4">
                      <FormGroup>
                        <Label htmlFor="fourthKeyword">4. Keyword</Label>
                        <Input
                          type={this.state.userInfosForm.keywordsCard.inputs.fourthKeyword.type}
                          id="fourthKeyword"
                          placeholder={this.state.userInfosForm.keywordsCard.inputs.fourthKeyword.placeholder}
                          value={this.state.userInfosForm.keywordsCard.inputs.fourthKeyword.value}
                          onChange={(event) => this.inputChangedHandler(event, 'keywordsCard', 'fourthKeyword')}/>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="4">
                      <FormGroup>
                        <Label htmlFor="fifthKeyword">5. Keyword</Label>
                        <Input
                          type={this.state.userInfosForm.keywordsCard.inputs.fifthKeyword.type}
                          id="fifthKeyword"
                          placeholder={this.state.userInfosForm.keywordsCard.inputs.fifthKeyword.placeholder}
                          value={this.state.userInfosForm.keywordsCard.inputs.fifthKeyword.value}
                          onChange={(event) => this.inputChangedHandler(event, 'keywordsCard', 'fifthKeyword')}/>
                      </FormGroup>
                    </Col>
                  </FormGroup>
                </CollideasFormCard>
              </Col>
            </Row>

            <Row>
              <Col xs="12">
                <CollideasFormCard
                  cardHeader={this.state.userInfosForm.collaborationCard.cardHeader}
                  isOpen={this.state.userInfosForm.collaborationCard.isOpen}
                  toggle={() => this.toggle('collaborationCard')}
                  isAction>
                  <FormGroup row className="my-0">
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="agriculturalScience" >Agricultural Science</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.agriculturalScience.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'agriculturalScience')}
                            size={'sm'}
                            id={'agriculturalScience'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="economics" >Economics</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.economics.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'economics')}
                            size={'sm'}
                            id={'economics'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="law" >Law</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.law.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'law')}
                            size={'sm'}
                            id={'law'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="physics" >Physics</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.physics.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'physics')}
                            size={'sm'}
                            id={'physics'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="anthropology" >Anthropology</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.anthropology.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'anthropology')}
                            size={'sm'}
                            id={'anthropology'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="education" >Education</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.education.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'education')}
                            size={'sm'}
                            id={'education'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="linguistics" >Linguistics</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.linguistics.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'linguistics')}
                            size={'sm'}
                            id={'linguistics'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="politicalScience" >Political Science</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.politicalScience.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'politicalScience')}
                            size={'sm'}
                            id={'politicalScience'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="biology" >Biology</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.biology.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'biology')}
                            size={'sm'}
                            id={'biology'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="engineering" >Engineering</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.engineering.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'engineering')}
                            size={'sm'}
                            id={'engineering'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="literature" >Literature</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.literature.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'literature')}
                            size={'sm'}
                            id={'literature'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="psychology" >Psychology</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.psychology.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'psychology')}
                            size={'sm'}
                            id={'psychology'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="chemistry" >Chemistry</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.chemistry.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'chemistry')}
                            size={'sm'}
                            id={'chemistry'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="entertainmentAndArts" >Entertainment and Arts</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.entertainmentAndArts.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'entertainmentAndArts')}
                            size={'sm'}
                            id={'entertainmentAndArts'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="mathematics" >Mathematics</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.mathematics.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'mathematics')}
                            size={'sm'}
                            id={'mathematics'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="socialSciences" >Social Sciences</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.socialSciences.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'socialSciences')}
                            size={'sm'}
                            id={'socialSciences'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="computerSciences" >Computer Sciences</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.computerSciences.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'computerSciences')}
                            size={'sm'}
                            id={'computerSciences'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="geoscience" >Geoscience</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.geoscience.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'geoscience')}
                            size={'sm'}
                            id={'geoscience'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="medicine" >Medicine</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.medicine.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'medicine')}
                            size={'sm'}
                            id={'medicine'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="spaceSicence" >Space Science</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.spaceSicence.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'spaceSicence')}
                            size={'sm'}
                            id={'spaceSicence'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="design" >Design</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.design.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'design')}
                            size={'sm'}
                            id={'design'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="history" >History</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.history.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'history')}
                            size={'sm'}
                            id={'history'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="philosophy" >Philosophy</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.philosophy.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'philosophy')}
                            size={'sm'}
                            id={'philosophy'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="other" >Other</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.collaborationCard.inputs.other.value}
                            onChange={(event) => this.inputChangedHandler(event, 'collaborationCard', 'other')}
                            size={'sm'}
                            id={'other'}/>
                        </div>
                      </FormGroup>
                    </Col>
                  </FormGroup>
                </CollideasFormCard>
              </Col>
            </Row>
            <Row>
              <Col xs="12">
                <CollideasFormCard
                  cardHeader={this.state.userInfosForm.availabilitySectorsCard.cardHeader}
                  isOpen={this.state.userInfosForm.availabilitySectorsCard.isOpen}
                  toggle={() => this.toggle('availabilitySectorsCard')}
                  isAction>
                  <FormGroup row className="my-0">
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="academia" >Academia</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.availabilitySectorsCard.inputs.academia.value}
                            onChange={(event) => this.inputChangedHandler(event, 'availabilitySectorsCard', 'academia')}
                            size={'sm'}
                            id={'academia'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="industry" >Industry</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.availabilitySectorsCard.inputs.industry.value}
                            onChange={(event) => this.inputChangedHandler(event, 'availabilitySectorsCard', 'industry')}
                            size={'sm'}
                            id={'industry'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="government" >Government</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.availabilitySectorsCard.inputs.government.value}
                            onChange={(event) => this.inputChangedHandler(event, 'availabilitySectorsCard', 'government')}
                            size={'sm'}
                            id={'government'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="civilSociety" >Civil Society</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.availabilitySectorsCard.inputs.civilSociety.value}
                            onChange={(event) => this.inputChangedHandler(event, 'availabilitySectorsCard', 'civilSociety')}
                            size={'sm'}
                            id={'civilSociety'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="2" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="allSectors" >All</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.availabilitySectorsCard.inputs.allSectors.value}
                            onChange={(event) => this.inputChangedHandler(event, 'availabilitySectorsCard', 'allSectors')}
                            size={'sm'}
                            id={'allSectors'}/>
                        </div>
                      </FormGroup>
                    </Col>
                  </FormGroup>
                </CollideasFormCard>
              </Col>
            </Row>

            <Row>
              <Col xs="12" sm="6">
                <CollideasFormCard
                  cardHeader={this.state.userInfosForm.dataCard.cardHeader}
                  isOpen={this.state.userInfosForm.dataCard.isOpen}
                  toggle={() => this.toggle('dataCard')}
                  isAction>
                  <FormGroup row className="my-0">
                    <Col xs="6" sm="6" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="available" >Available</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.dataCard.inputs.available.value}
                            onChange={(event) => this.inputChangedHandler(event, 'dataCard', 'available')}
                            size={'sm'}
                            id={'available'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="6" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="toBeCollected" >To be collected</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.dataCard.inputs.toBeCollected.value}
                            onChange={(event) => this.inputChangedHandler(event, 'dataCard', 'toBeCollected')}
                            size={'sm'}
                            id={'toBeCollected'}/>
                        </div>
                      </FormGroup>
                    </Col>
                  </FormGroup>
                </CollideasFormCard>
              </Col>
              <Col xs="12" sm="6">
                <CollideasFormCard
                  cardHeader={this.state.userInfosForm.methodCard.cardHeader}
                  isOpen={this.state.userInfosForm.methodCard.isOpen}
                  toggle={() => this.toggle('methodCard')}
                  isAction>

                  <FormGroup row className="my-0">
                    <Col xs="6" sm="3" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="quantitative" >Quantitative</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.methodCard.inputs.quantitative.value}
                            onChange={(event) => this.inputChangedHandler(event, 'methodCard', 'quantitative')}
                            size={'sm'}
                            id={'quantitative'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="3" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="qualitative" >Qualitative</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.methodCard.inputs.qualitative.value}
                            onChange={(event) => this.inputChangedHandler(event, 'methodCard', 'qualitative')}
                            size={'sm'}
                            id={'qualitative'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="3" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="mixed" >Mixed</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.methodCard.inputs.mixed.value}
                            onChange={(event) => this.inputChangedHandler(event, 'methodCard', 'mixed')}
                            size={'sm'}
                            id={'mixed'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="3" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="allMethods" >All</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.methodCard.inputs.allMethods.value}
                            onChange={(event) => this.inputChangedHandler(event, 'methodCard', 'allMethods')}
                            size={'sm'}
                            id={'allMethods'}/>
                        </div>
                      </FormGroup>
                    </Col>
                  </FormGroup>
                </CollideasFormCard>
              </Col>
            </Row>

            <Row>
              <Col xs="12" sm="6">
                <CollideasFormCard
                  cardHeader={this.state.userInfosForm.scopeOfInvolvementCard.cardHeader}
                  isOpen={this.state.userInfosForm.scopeOfInvolvementCard.isOpen}
                  toggle={() => this.toggle('scopeOfInvolvementCard')}
                  isAction>
                  <FormGroup row className="my-0">
                    <Col xs="6" sm="3" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="overall" >Overall</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.scopeOfInvolvementCard.inputs.overall.value}
                            onChange={(event) => this.inputChangedHandler(event, 'scopeOfInvolvementCard', 'overall')}
                            size={'sm'}
                            id={'overall'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="3" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="literature" >Literature</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.scopeOfInvolvementCard.inputs.literature.value}
                            onChange={(event) => this.inputChangedHandler(event, 'scopeOfInvolvementCard', 'literature')}
                            size={'sm'}
                            id={'literature'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="3" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="analysis" >Analysis</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.scopeOfInvolvementCard.inputs.analysis.value}
                            onChange={(event) => this.inputChangedHandler(event, 'scopeOfInvolvementCard', 'analysis')}
                            size={'sm'}
                            id={'analysis'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="3" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="interpretation" >Interpretation</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.scopeOfInvolvementCard.inputs.interpretation.value}
                            onChange={(event) => this.inputChangedHandler(event, 'scopeOfInvolvementCard', 'interpretation')}
                            size={'sm'}
                            id={'interpretation'}/>
                        </div>
                      </FormGroup>
                    </Col>
                  </FormGroup>
                </CollideasFormCard>
              </Col>
              <Col xs="12" sm="6">
                <CollideasFormCard
                  cardHeader={this.state.userInfosForm.sequencingCard.cardHeader}
                  isOpen={this.state.userInfosForm.sequencingCard.isOpen}
                  toggle={() => this.toggle('sequencingCard')}
                  isAction>

                  <FormGroup row className="my-0">
                    <Col xs="6" sm="4" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="firstAuthor" >First Author</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.sequencingCard.inputs.firstAuthor.value}
                            onChange={(event) => this.inputChangedHandler(event, 'sequencingCard', 'firstAuthor')}
                            size={'sm'}
                            id={'firstAuthor'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="4" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="decideLater" >Decide Later</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.sequencingCard.inputs.decideLater.value}
                            onChange={(event) => this.inputChangedHandler(event, 'sequencingCard', 'decideLater')}
                            size={'sm'}
                            id={'decideLater'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="4" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="doesnotmatter" >Doesn't matter</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.sequencingCard.inputs.doesnotmatter.value}
                            onChange={(event) => this.inputChangedHandler(event, 'sequencingCard', 'doesnotmatter')}
                            size={'sm'}
                            id={'doesnotmatter'}/>
                        </div>
                      </FormGroup>
                    </Col>
                  </FormGroup>

                </CollideasFormCard>
              </Col>
            </Row>

            <Row>
              <Col xs="12" sm="6">
                <CollideasFormCard
                  cardHeader={this.state.userInfosForm.numberOfCoauthorsCard.cardHeader}
                  isOpen={this.state.userInfosForm.numberOfCoauthorsCard.isOpen}
                  toggle={() => this.toggle('numberOfCoauthorsCard')}
                  isAction>

                  <FormGroup row className="my-0">

                  </FormGroup>

                </CollideasFormCard>
              </Col>
              <Col xs="12" sm="6">
                <CollideasFormCard
                  cardHeader={this.state.userInfosForm.timeFrameCard.cardHeader}
                  isOpen={this.state.userInfosForm.timeFrameCard.isOpen}
                  toggle={() => this.toggle('timeFrameCard')}
                  isAction>

                  <FormGroup row className="my-0">
                    <Col xs="6" sm="4" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="oneToThreeMonths" >1-3 months</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.timeFrameCard.inputs.oneToThreeMonths.value}
                            onChange={(event) => this.inputChangedHandler(event, 'timeFrameCard', 'oneToThreeMonths')}
                            size={'sm'}
                            id={'oneToThreeMonths'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="4" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="sixMonthsToOneYear" >6 months - 1 year</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.timeFrameCard.inputs.sixMonthsToOneYear.value}
                            onChange={(event) => this.inputChangedHandler(event, 'timeFrameCard', 'sixMonthsToOneYear')}
                            size={'sm'}
                            id={'sixMonthsToOneYear'}/>
                        </div>
                      </FormGroup>
                    </Col>
                    <Col xs="6" sm="4" className={'text-center'}>
                      <FormGroup>
                        <div>
                          <Label htmlFor="onePlusYear" >1+ year</Label>
                        </div>
                        <div>
                          <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            checked={this.state.userInfosForm.timeFrameCard.inputs.onePlusYear.value}
                            onChange={(event) => this.inputChangedHandler(event, 'timeFrameCard', 'onePlusYear')}
                            size={'sm'}
                            id={'onePlusYear'}/>
                        </div>
                      </FormGroup>
                    </Col>
                  </FormGroup>

                </CollideasFormCard>
              </Col>
            </Row>
            <Row >
              <Col xs="4" sm="10"/>
              <Col xs="2" sm="2">
                <Button type="submit" color="primary">Save changes</Button>
              </Col>
              <Col xs="4" sm="0"/>
            </Row>
          </Form>
        </Fade>
        <div style={{height: '7px' }} ></div>


      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null
  };
};

export default connect(mapStateToProps, null)  (MyUserInfosForm);
