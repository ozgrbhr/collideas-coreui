import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';

class Home extends Component {

  render() {

    return (
      <div className="animated fadeIn">
        <Row>
          <Col sm="12" md={{ size: 8, offset: 2 }}>
            <Card >
              <CardBody>
                <div className="text-center">
                  <h2>Creative Collisions Accelerating Novelty!</h2>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xs="6" sm="4">
            <Card >
              <CardBody>
                <div className="text-center">
                  <h4><u>Discover</u></h4>
                  <p>Discover your potential co-authors from academia, industry, public administrations, civil society organizations, as well as  other parties, and be discovered!</p>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col xs="6" sm="4">
            <Card >
              <CardBody>
                <div className="text-center">
                  <h4><u>Connect</u></h4>
                  <p>Connect with your potential co-authors openly and transparently regarding your research priorities and preferences.</p>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col sm="4">
            <Card >
              <CardBody>
                <div className="text-center">
                  <h4><u>Engage</u></h4>
                  <p>Engage with your potential co-authors effectively and efficiently.</p>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col xs="6" sm="4">
            <Card >
              <CardBody>
                <div className="text-center">
                  <h4><u>Collaborate</u></h4>
                  <p>Collaborate to accelerate the disciplinary, interdisciplinary, multidisciplinary, transdisciplinary processes for your research.</p>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col xs="6" sm="4">
            <Card >
              <CardBody>
                <div className="text-center">
                  <h4><u>Create</u></h4>
                  <p>Create co-authored research with other people in your city, country, across the world for free, even raise funds.</p>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col sm="4">
            <Card >
              <CardBody>
                <div className="text-center">
                  <h4><u>Go</u></h4>
                  <p>Go to suggested conferences, workshops , symposiums, and submit your co-created research to journals and publishers.</p>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>

        </Row>
      </div>
    )
  }
}

export default Home;
